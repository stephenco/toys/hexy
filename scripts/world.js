const uiState = {
  normal: 'normal',
  hover: 'hover',
  focussed: 'focussed',
  selected: 'selected',
  related: 'related',
  related2: 'related2',
  considered: 'consideration',
  considered2: 'consideration2'
};
const hexSize = 20;
const fieldWidth = Math.floor(800 / (hexSize * 3.1));
const fieldHeight = Math.floor(600 / (hexSize));

class Hex {
  constructor(column, row, size) {
    this.column = column;
    this.row = row;
    this.size = size || hexSize;
    this.isEvenRow = (row % 2 == 0);
    this.state = uiState.normal;
  }

  get position() {
    const canvasOffset = {
      x: this.size * 1.1,
      y: this.size
    };
    const angleOffset = {
      x: this.isEvenRow ? 0 : this.size * 1.5,
      y: this.size * (-0.65 * this.row)
    };
    return {
      x: canvasOffset.x + (this.column * (this.size * 3)) + angleOffset.x, //(this.size * (this.isEvenRow ? 1 : 2)),
      y: canvasOffset.y + (this.row * (this.size * 1.5)) + angleOffset.y, //this.size
    };
  }

  get hitCoordinates() {
    const pos = this.position;
    return this.shrinkCoordinates(
      pos.x - this.size,
      pos.y - this.size,
      pos.x + this.size,
      pos.y + this.size,
      4
    );
  }

  get siblingCoordinates() {
    return {
      top: { row: this.row - 2, column: this.column },
      bottom: { row: this.row + 2, column: this.column },
      topLeft: { row: this.row - 1, column: this.isEvenRow? this.column - 1 : this.column },
      topRight: { row: this.row - 1, column: this.isEvenRow ? this.column : this.column + 1 },
      bottomLeft: { row: this.row + 1, column: this.isEvenRow ? this.column - 1 : this.column },
      bottomRight: { row: this.row + 1, column: this.isEvenRow ? this.column : this.column + 1 }
    };
  }

  shrinkCoordinates(x1, y1, x2, y2, px) {
    return {
      x1: x1 + px,
      y1: y1 + px,
      x2: x2 - px,
      y2: y2 - px
    };
  }

  checkCollision(x1, y1, x2, y2) {
    const c = this.hitCoordinates;
    return !(c.x1 > x2 || c.x2 < x1 || c.y1 > y2 || c.y2 < y1);
  }

  drawPolygon(context, sides, hasStroke, hasFill) {
    const size = this.size;
    const pos = this.position;

    context.beginPath();
    context.moveTo (pos.x +  size * Math.cos(0), pos.y +  size *  Math.sin(0));

    for (var i = 1; i <= sides; i += 1) {
        context.lineTo (pos.x + size * Math.cos(i * 2 * Math.PI / sides), pos.y + size * Math.sin(i * 2 * Math.PI / sides));
    }

    context.stroke();
    context.fill();
  }

  draw(context) {
    const map = {
      normal: {
        stroke: '#fff',
        fill: '#9ca'
      },

      selected: {
        fill: '#123',
      },

      focussed: {
        fill: '#def'
      },

      hover: {
        fill: '#fc9'
      },

      related: {
        fill: '#69c'
      },

      related2: {
        fill: '#fca'
      },

      consideration: {
        fill: '#fc9'
      },

      consideration2: {
        fill: '#bca'
      }
    };

    const colors = map[this.state] || map[uiState.normal];
    const defaultColors = map[uiState.normal];

    context.save();
    context.strokeStyle = colors.stroke || defaultColors.stroke;
    context.fillStyle = colors.fill || defaultColors.fill;
    context.lineWidth = 1;
    this.drawPolygon(context, 6, 1, 1);
    context.restore();
    return this;
  }
}

export default class World {
  constructor(canvas) {
    this.canvas = canvas;
    this.drawingContext = canvas.getContext('2d');
    this.hasChanged = true;
    this.fieldWidth = fieldWidth;
    this.fieldHeight = fieldHeight;
    this.field = [];

    for (let y = 0; y < this.fieldHeight; y++) {
      for (let x = 0; x < this.fieldWidth; x++) {
        this.field[y] = this.field[y] || [];
        this.field[y].push(new Hex(x, y));
      }
    }

    canvas.addEventListener('mousemove', e => {
      const rect = canvas.getBoundingClientRect();
      this.mouseMoved(e.clientX - rect.left, e.clientY - rect.top);
    });

    canvas.addEventListener('click', e => {
      this.mouseClicked();
    });

    window.addEventListener('keypress', e => {
      this.started ? this.stop() : this.start();
      console.log('key pressed ' + e.charCode + ': ' + (this.started ? 'unpaused' : 'paused'));
    });
  }

  mouseClicked() {
    if (this.selected) {
      console.log(this.selected);
      this.selected.state = this.selected.state == uiState.focussed ? uiState.selected : uiState.focussed;
      this.hasChanged = true;
    }
  }

  mouseMoved(x, y) {
    if (!this.started) { return; }
    this.mouse = { x: x, y: y };
    this.hoverField(this.mouse);
  }

  hoverField(mouse) {
    if (this.selected && this.selected.checkCollision(mouse.x, mouse.y, mouse.x, mouse.y)) {
      return;
    }

    for (let touched of this.touched || []) {
      if (!touched) { continue; }
      touched.state = uiState.normal;
    }

    const searchY = Math.floor((mouse.y / 600) * (fieldHeight * 1.2));
    const minY = Math.max(0, searchY - 2), maxY = Math.min(fieldHeight - 1, searchY + 2);

    const searchX = Math.floor((mouse.x / 800) * fieldWidth);
    const minX = Math.max(0, searchX), maxX = Math.min(fieldWidth - 1, searchX + 1);

    //console.log('looking for hover at: ' + minX + '-' + maxX + ' X ' + minY + '-' + maxY);
    this.touched = [];

    let done = false;
    for (let y = minY; y <= maxY; y++) {
      for (let x = minX; x <= maxX; x++) {
        const item = ((this.field[y] || [])[x] || null);
        if (!item) { continue; }
        if (item.checkCollision(mouse.x, mouse.y, mouse.x, mouse.y)) {
          this.selected = item;
          done = true;
        } else {
          item.state = done ? uiState.considered2 : uiState.considered;
          this.touched.push(item);
        }
      }
    }

    if (this.selected) {
      const related = (row, column, ver) => {
        const x = column;
        const y = row;
        if (x >= 0 && y >= 0 && x < this.fieldWidth && y < this.fieldHeight) {
          const item = this.field[y][x];
          if (!item) { return null; }
          item.state = ver ? uiState.related + ver : uiState.related;
          this.touched.push(item);
          return item;
        }
        return null;
      }

      this.touched.push(this.selected);
      this.selected.state = uiState.focussed;
      const sibs = this.selected.siblingCoordinates;

      related(sibs.top.row, sibs.top.column);
      related(sibs.bottom.row, sibs.bottom.column);
      related(sibs.topLeft.row, sibs.topLeft.column);
      related(sibs.topRight.row, sibs.topRight.column);
      related(sibs.bottomLeft.row, sibs.bottomLeft.column);
      related(sibs.bottomRight.row, sibs.bottomRight.column);
    }

    this.hasChanged = true;
  }

  connect(x1, y1, x2, y2) {
    this.hasChanged = true;
    this.selected = null;

    for (let touched of this.touched || []) {
      if (!touched) { continue; }
      touched.state = uiState.normal;
    }

    this.touched = [];

    const to = (this.field[y2] || [])[x2];
    if (!to) {
      console.log("can't get to " + x2 + " x " + y2);
      return;
    }

    const from = (this.field[y1] || [])[x1];
    if (!from) {
      console.log("can't get to " + x1 + " x " + y1);
      return;
    }

    to.state = uiState.focussed;
    from.state = uiState.focussed;
    this.touched.push(to);
    this.touched.push(from);

    let steps = 0;
    let done = false;
    let current = { row: from.row, column: from.column };

    while (!done && steps++ < 100) {
      const cc = (this.field[current.row] || [])[current.column];
      if (!cc) {
        console.warn("can't get to " + current.column + " X " + current.row + ". Interrupted.");
        break;
      }
      const c = cc.siblingCoordinates;

      if (current.row < to.row && current.column < to.column) {
        current = { row: c.bottomRight.row, column: c.bottomRight.column };
      }
      else if (current.row < to.row && current.column > to.column) {
        current = { row: c.bottomLeft.row, column: c.bottomLeft.column };
      }
      else if (current.row < to.row) {
        current = !cc.isEvenRow ? { row: c.bottom.row, column: c.bottom.column } : { row: c.bottomRight.row, column: c.bottomRight.column };
      }
      else if (current.row > to.row && current.column < to.column) {
        current = { row: c.topRight.row, column: c.topRight.column };
      }
      else if (current.row > to.row && current.column > to.column) {
        current = { row: c.topLeft.row, column: c.topLeft.column };
      }
      else if (current.row > to.row) {
        current = cc.isEvenRow ? { row: c.top.row, column: c.top.column } : { row: c.topLeft.row, column: c.topLeft.column };
      }
      else if (current.column < to.column) {
        current = { row: c.bottomRight.row, column: c.bottomRight.column };
      }
      else if (current.column > to.column) {
        current = { row: c.bottomLeft.row, column: c.bottomLeft.column };
      }
      else if (current.column == to.column && current.row == to.row) {
        console.log('found path in ' + steps + ' steps');
        done = true;
      }
      else {
        console.warn('unhandled turn in pathfinding', current, to);
        break;
      }

      const next = (this.field[current.row] || [])[current.column];
      if (next && next != to) {
        next.state = uiState.related;
        this.touched.push(next);
      }
    }

    if (steps >= 100) {
      console.warn('failed to find path in ' + (steps - 1) + ' steps');
    }

    return this.start();
  }

  start() {
    if (this.started) { return; }
    this.started = true;

    const self = this;
    (function fn(){
      if (!self.started) { return; }
      self.draw(self.drawingContext);
      requestAnimationFrame(fn);
    })();

    return this;
  }

  stop() {
    this.started = false;
    return this;
  }

  draw() {
    if (!this.hasChanged || !this.started) { return; }
    this.hasChanged = false;
    for (let row of this.field) {
      for (let column of row) {
        column.draw(this.drawingContext);
      }
    }
  }

}
