# Purpose

Experiment with drawing fields of hexagons on a grid, selecting them,
and understanding nearby relationships.

## Usage

`npm run dev`

Hosted at localhost:8080 by default.
Hover over spaces and select by clicking on them.
Observe output in the browser script console.

Can also get a reference to `world` in console and inspect its bits.
Use the `world` reference to invoke `world.connect( xx, yy )` to try finding
direct paths between two points. There are some issues with this.

## Issues

This was quick and dirty code used for POC and experimentation and could be
simplified and organized much better than it is. Also, the logic involved in
various pieces has problems and could improved.